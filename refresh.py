# Python has several structures to store collections or multiple items in a single variable
# List, Disctionary, Tuples and Set

# [Section] Lists
# Lists are similar to arrays of JS in a sense that they can contain a collection of data
# To create a list, the square bracket([]) is used.

names = ["John", "George", "Ringo", "Grim"] #String list
programs = ["developer career", "pi-shape", "short courses"] #string list
durations = [260,180,20] #number list

# Getting the list size
# The number of elements in a list can becounted using the len() method
print(len(programs))

# Accessing values/elements in a list
print(names[0])

# Accessing the last element in the array
print(names[-1])

# Accessing the second element in the array
print(names[1])

# Accessing the whole list
print(names)

# Accessing a range of values. It excludes the specified element, in this instance, the 3rd element. It will only display elements 0 and 1
print(names[0:2])

# Updating list
print(f"Current val: {programs[2]}")

programs[2] = "Short"
print(f"Updated val: {programs[2]}")

# [Section] List Manipulation
# Adding a new element in List
programs.append("global")
print(programs)

# To delete a list items - "del" keyword is used to delete an element
durations.append(360)
print(durations)

# Deleting the last element
del durations[-1]
print(durations)

# Deleting the first element of the array
del durations[0]
print(durations)

# Insert method - First argument is the reference and the 2nd argument is the element you want to add
durations.insert(0, 100)
print(durations)
durations.append(360)
print(durations)

# Inserting an element in the middle of the array by calculating the length of the array and then dividing it by 2 in order to get the middle value as the reference when inserting the value in the first argument of the insert and since dividing the value returns a float, I then have to convert it to int in order to get a whole number
durations.insert(int(len(durations) / 2), 5)
print(durations)
durations.insert(int(len(durations) / 2), 5)
print(durations)
durations.insert(int(len(durations) / 2), 10)
print(durations)

# Membership checking which is equivalent to "includes" in Javascript which returns true or false
# In Python, its the keyword "in"
print(5 in durations)

# Sorting lists using "sort()" is ascending by default
durations.sort()
print(durations)

# Emptying a list contents using the "clear()" method
durations.clear();
print(durations)

# [Dictionaries]
person1 = {
    "name" : "Brandon",
    "age" : 25,
    "occupation": "student",
    "isEnrolled" : True,
    "subjects": ["Python", "SQL", "Django"]
}

# Count the number of key-value pairs in dictionary/object
print(len(person1))

# To get item in dictionary, you can use a bracket "[]"
print(person1["name"])

# Accessing a nested item inside a dictionary. For instance, if we want to access Django which is the last lement in the property subjects
print(person1["subjects"][-1])

# The "keys()" method will return a list of all the keys in the dictionary
print(person1.keys())

# The "values()" method will return a list of all the values in the dictionary
print(person1.values())

# The "items()" method will return item in a dictionary as key-value pair in a list
print(person1)
print(person1.items())

# Adding key-value pairs can be done with either a new index key and assigning value or the update method
# index key
person1["nationality"] = "Filipino"
print(person1)

# update method
person1.update({"fav_food": "Sinigang"})
print(person1)

# Deleting entries using the "pop()" method and the "del" keyword
# pop(<item you want to delete>)
person1.pop("name")
print(person1)

# del keyword
del person1["subjects"]
print(person1)


person3 = {
	"name": "Monika",
	"age" : 20,
	"occupation": "poet",
	"isEnrolled": True,
	"subjects" : ["Python", "SQL", "Django"]
}


student1 = "student1"
student2 = "student2"
class_room = {
	student1 : person1,
	student2 : person3
}

print(class_room["student2"]["subjects"][1])





