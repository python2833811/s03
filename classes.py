# [Section] Functions
# Functions are block of code that runs when called.
# A function can be used to get inputs, process inputs and return inputs
# The "def" keyword is used to create a function. The syntax:
#  def <function_name>()

# defines a function called my_greeting
def my_greeting():
	print("Hello user!")

# Calling or Invoking a function - To use a function, just specify the function name and provide the value/values needed if there are.
my_greeting()

# Parameters can be added to functions to have more control to what the inputs for the function will be.

def greet_user(username):
	print(f'Hello, {username}')



# Arguments are the vlues that are submitted to the parameters
greet_user("Chris")

# Return statements - the "return" keyword allow functions to return values

def addition(num1, num2):
	return num1 + num2

sum = addition(5, 9)
print(f'The sum is {sum}!')
print(addition(5,9))

# [Section] Lambda Functions
# A lambda function is a small anonymous function that can be used for callbacks
# It is just like any normal python function, except that it has no name when defining it, and it is contained in one of code

# A lambda funciton can take any number of arguments but can only have one expression

greeting = lambda person: f'hello {person}'

greet = greeting("Elsie")
print(greet)

mult = lambda a, b : a * b

print(mult(5, 6))

# [Section] Classes
# Classes would serve as blueprints to describe the concept of objects
# Each Object has characteristics (properties) and behaviors (method)

# To create a Class, the "class" keyword is used along with the class name that starts with an uppercase character

class Car():
	# properties that all car Car objects must have are defined in a method called init

	# initializing the properties of our function
	def __init__(self, brand, model, year_of_make):	
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# propeties that are hard coded
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# method
	def fill_fuel(self):
		print(f'Current fuel level: {self.fuel_level}.')
		print('filing up the fuel tank . . .')
		self.fuel_level = 100
		print(f'New fuel level: {self.fuel_level}.')


	def drive(self, distance):
		print(f'The car has driven {distance} kilometers!')

		self.fuel_level = self.fuel_level - distance

		print(f'The fuel left: {self.fuel_level}')




# intantiate a class
new_car = Car("Nissan", "GT-R", 2019)

print(new_car.brand)
print(new_car.model)
print(new_car.year_of_make)

print(new_car)
# to invoke a method
new_car.fill_fuel()
print(new_car.fuel_level)

new_car.drive(50)
print(new_car.fuel_level)
