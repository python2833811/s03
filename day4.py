# Section function - use "def" keyword in order to create a function
def greetUser(username):
	print(f"Hello {username}")

greetUser("Aisha")

def addition(num1,num2):
	return num1+num2

print(addition(2,2))

# [Section] Lambda functions - creates an anonymous function that can be used for callbacks
# lambda function can take any number of arguments but can only have one expression
greeting = lambda person: print(f"hello {person}")
greeting("test")

mult = lambda a,b: a *b
print(mult(2,2))

# [Section] Classes
