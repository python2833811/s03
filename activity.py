yearInput = int(input("Enter year:\n"))

if yearInput % 4 == 0:
    if yearInput % 100 == 0:
        if yearInput % 400 == 0:
            print("Leap year")
        else:
            print("Not a leap year")
    else:
        print("Leap year")
else:
    print("Not Leap year")


row = int(input("Enter rows:\n"))
col = int(input("Enter col:\n"))

for y in range(row):
    print("*" * col)

